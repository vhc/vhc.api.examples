const config = require("./config");
const axios = require("axios");
const moment = require("moment");

axios.defaults.baseURL = config.url;
axios.defaults.headers.common["Content-Type"] = "application/json";
axios.defaults.headers.common["Accept"] = "application/json";
const http = axios.create({
  baseURL: config.url,
  timeout: 30000,
  headers: { "Content-Type": "application/json", Accept: "application/json" }
});

let propertyId = '1_240219'; // To get the Property IDs to use, please use the 'api/Property/List' method first. (you can use the propertyList.js as reference)
let checkinDate = moment("2020-05-14");
let checkoutDate = moment("2020-05-24");

/* MAIN APPLICATION CONTENT */
run_application();

/* FUNCTIONS */

async function run_application() {
    console.log(`\n--- Checking if property ${propertyId} is available to book between `+
                `${checkinDate.format("YYYY-MM-DD")} and ${checkoutDate.format("YYYY-MM-DD")}, `+ 
                `plus showing all the unavailable dates for this property...\n`);

    let propertyAvailabilityResult = await checkPropertyAvailability();
    console.log(`--- [PROPERTY AVAILABLE TO BOOK] Property ${propertyId} is ${(propertyAvailabilityResult.isAvailable ? 'AVAILABLE' : 'NOT AVAILABLE')} to be booked in this date interval!\n`);

    let propertyUnavailableDatesResult = await checkPropertyUnavailableDates();
    console.log(`--- [UNAVAILABLE DATES] Found ${propertyUnavailableDatesResult.length} ${(propertyUnavailableDatesResult.length > 1 ? 'dates' : 'date')} in total:`);
    propertyUnavailableDatesResult.forEach(item => {
      console.log(`-> DATE: ${item.dateBooked.replace('T00:00:00', '')}`);
    });
}

async function checkPropertyAvailability() {
  let loginRequest = await doLogin();
  axios.defaults.headers.common["Authorization"] = `Bearer ${loginRequest.apiToken}`;

  let isAvailable = await axios.get(`api/Property/IsAvailable/${propertyId}?checkinDate=${checkinDate.format("YYYY-MM-DD")}&checkoutDate=${checkoutDate.format("YYYY-MM-DD")}`);
  return isAvailable.data;
}

async function checkPropertyUnavailableDates() {
  let loginRequest = await doLogin();
  axios.defaults.headers.common["Authorization"] = `Bearer ${loginRequest.apiToken}`;

  let unavailableDates = await axios.get(`api/Property/GetReservedDatesByProperty/${propertyId}`);
  return unavailableDates.data;
}

async function doLogin() {
  let res = await axios.post("api/Login", {
    userName: config.login,
    password: config.password,
    authenticationFrom: 1
  });

  return res.data;
}
