const config = require("./config");
const axios = require("axios");
const moment = require("moment");

axios.defaults.baseURL = config.url;
axios.defaults.headers.common["Content-Type"] = "application/json";
axios.defaults.headers.common["Accept"] = "application/json";
const http = axios.create({
  baseURL: config.url,
  timeout: 30000,
  headers: { "Content-Type": "application/json", Accept: "application/json" }
});

let startBookingBody = {
  propertyid: "1_240219",
  checkin: "2020-05-14",
  checkout: "2020-05-24",
  language: "0", // 0 = ENGLISH, 1 = PORTUGUESE, 2 = ESPANOL -> Information described in documentation
  currencywished: "USD", // USD or BRL
  firstname: "Test",
  lastname: "User",
  phone: "14075551234",
  email: "test@test.com",
  message: "This is a test message",
  address: "Test Street, 1234",
  zipcode: "12345",
  city: "Los Angeles",
  state: "California",
  country: "USA",
  bookingid: ""
};

/* MAIN APPLICATION CONTENT */
run_application();

/* FUNCTIONS */

async function run_application() {
    console.log(`\n--- Booking property ${startBookingBody.propertyid} with checkin date `+
                `${startBookingBody.checkin} and checkout date ${startBookingBody.checkout}...\n`);

    console.log('--- The booking process works in three steps:');
    console.log('1) You must execute the StartBooking method, which will start a booking in our system. This process gives you a "bookingID" and keeps the selected dates "reserved" for 15 minutes;');
    console.log('2) To finish the booking process, you must use the MakeBooking, sending the same object sent in the first step, but now with the "bookingID" filled with the ID obtained in the first step;');
    console.log('3) If you haven\'t finished your booking process (MakeBooking), you can cancel the booking manually using the "bookingID" obtained in the first step.\n');

    console.log('--- [START BOOKING] Starting the booking reservation...');
    let startBooking = await funcStartBooking();
    console.log(`--- [START BOOKING] The booking for property ${startBookingBody.propertyid} is ${(startBooking.bookingPlaced > 0 ? `PLACED, with the Booking ID ${startBooking.bookingID}` : 'NOT PLACED. Please check the request sent and try again')}!\n`);
    startBookingBody.bookingID = startBooking.bookingID;

    if(startBooking.bookingPlaced > 0) {
      console.log(`--- [MAKE BOOKING] Confirming the booking ${startBooking.bookingID}...`);
      let makeBooking = await funcMakeBooking();
      console.log(`--- [MAKE BOOKING] The booking for property ${startBookingBody.propertyid} is ${(makeBooking.bookingPlaced > 0 ? `CONFIRMED, with the Booking ID ${makeBooking.bookingID}` : 'NOT CONFIRMED. Please check the request sent and try again')}!\n`);
    }

    if(startBooking.bookingPlaced > 0) {
      console.log(`--- [CANCEL BOOKING] Cancelling the booking ${startBooking.bookingID}...`);
      let cancelBooking = await funcCancelBooking();
      console.log(`--- [CANCEL BOOKING] Booking ID ${startBooking.bookingID} - Action Result: ${cancelBooking.bookingCancelResults[Object.keys(cancelBooking.bookingCancelResults)[0]]}\n`);
    }
}

async function funcStartBooking() {
  let loginRequest = await doLogin();
  axios.defaults.headers.common["Authorization"] = `Bearer ${loginRequest.apiToken}`;

  let request = await axios.post(`api/Booking/StartBooking`, startBookingBody);
  return request.data;
}

async function funcMakeBooking() {
  let loginRequest = await doLogin();
  axios.defaults.headers.common["Authorization"] = `Bearer ${loginRequest.apiToken}`;

  let request = await axios.post(`api/Booking/MakeBooking`, startBookingBody);
  return request.data;
}

async function funcCancelBooking() {
  let loginRequest = await doLogin();
  axios.defaults.headers.common["Authorization"] = `Bearer ${loginRequest.apiToken}`;

  let request = await axios.post(`api/Booking/CancelPendingBooking`, {
    bookingID: startBookingBody.bookingID
  });
  return request.data;
}

async function doLogin() {
  let res = await axios.post("api/Login", {
    userName: config.login,
    password: config.password,
    authenticationFrom: 1
  });

  return res.data;
}
