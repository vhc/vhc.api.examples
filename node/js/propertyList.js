const config = require("./config");
const axios = require("axios");
const moment = require("moment");

axios.defaults.baseURL = config.url;
axios.defaults.headers.common["Content-Type"] = "application/json";
axios.defaults.headers.common["Accept"] = "application/json";
const http = axios.create({
  baseURL: config.url,
  timeout: 30000,
  headers: { "Content-Type": "application/json", Accept: "application/json" }
});

let currency = 'USD';
let checkinDate = moment("2020-05-14");
let checkoutDate = moment("2020-05-24");
let zones = [
  // To get the Zone IDs to use, please use the 'api/Zone/List' method first.
  17, // Miami
  18 // Orlando
];

/* MAIN APPLICATION CONTENT */

run_application();

/* FUNCTIONS */

async function run_application() {
    console.log(`\n--- Searching for available properties between `+
                `${checkinDate.format("YYYY-MM-DD")} and ${checkoutDate.format("YYYY-MM-DD")}, `+ 
                `located in zones ${zones.join(" and ")}, only in currency ${currency}...\n`);

    let propertySearchResult = await searchProperties();

    console.log(`--- REQUEST SUCCESS! Found ${propertySearchResult.length} ${(propertySearchResult.length > 1 ? 'properties' : 'property')} in total:\n`);
    propertySearchResult.forEach(item => {
        console.log(`-> Property ID: ${item.idPmsExternal} | `+
                    `Community: ${item.name} | `+
                    `Price Base: $ ${item.pricePerNightBase.toFixed(2)} | `+
                    `Price calculated: ${(item.price ? `$ ${(item.price.total.toFixed(2))} [for ${item.price.numberOfNights} night(s)]` : 'not applicable')}`);
    });
}

async function searchProperties() {
  let loginRequest = await doLogin();
  axios.defaults.headers.common["Authorization"] = `Bearer ${loginRequest.apiToken}`;

  let properties = await axios.post("api/Property/List", {
    dateFrom: checkinDate.format("YYYY-MM-DD"),
    dateTo: checkoutDate.format("YYYY-MM-DD"),
    zones: zones,
    currencyWished: currency
  });

  return properties.data;
}

async function doLogin() {
  let res = await axios.post("api/Login", {
    userName: config.login,
    password: config.password,
    authenticationFrom: 1
  });

  return res.data;
}
