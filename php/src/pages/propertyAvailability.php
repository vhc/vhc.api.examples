<?php
include('includes/propertyService.php');

$propertyService = new PropertyService();
?>

<h1><?=$pageTitle;?></h1>

<p>In this page you can check if a property is available to be reserved.</p>
<p>Please use the Property List page to get the ID of the property you want to check.</p>

<form method="POST">
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label>Property ID <small><i>(Use 1_ at the start of the ID. Ex.: 1_123456)</i></small></label>
                <input type="text" class="form-control" id="propertyId" name="propertyId" value="<?=$_POST['propertyId'];?>">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Date From</label>
                <input type="date" class="form-control" id="checkInDate" name="checkInDate" value="<?=$_POST['checkInDate'];?>">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Date To</label>
                <input type="date" class="form-control" id="checkOutDate" name="checkOutDate" value="<?=$_POST['checkOutDate'];?>">
            </div>
        </div>
        <div class="col-lg-12 text-right">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
    </div>
</form>

<br />

<?php 
if($_POST['propertyId']) {
    $isAvailable = $propertyService->isPropertyAvailable($_POST['propertyId'], $_POST['checkInDate'], $_POST['checkOutDate']);
    $errorAvailable = $propertyService->getErrorFromResult($isAvailable);

    // $propertyService->printPre($isAvailable);
    $stringAvailable = (!empty($errorAvailable) ? '' 
                                                : ($isAvailable['isAvailable'] ? "<span class=\"font-weight-bold text-success\">AVAILABLE</span>" 
                                                                               : "<span class=\"font-weight-bold text-danger\">NOT AVAILABLE</span>"));
?>
    <div class="row">
        <div class="col-lg-12">
            <?php if(empty($errorAvailable)) { ?>
                <h4>From <?=$_POST['checkInDate'];?> to <?=$_POST['checkOutDate'];?>, the Property <?=$_POST['propertyId'];?> is <?=$stringAvailable;?> to be booked</h4>
            <?php } else { ?>
                <h4><span class="font-weight-bold text-danger">ERROR: </span><?=$errorAvailable;?></h4>
            <?php } ?>
        </div>
    </div>

<?php
    $notAvailableDates = $propertyService->getReservedDates($_POST['propertyId']);
?>

<div class="row">
    <div class="col-lg-12">
        <p>All dates unavailable for this property:</p>
    </div>
    <div class="col-lg-3">
        <table class="table table-responsive-lg text-center">
            <thead>
                <tr>
                    <th>Date</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($notAvailableDates as $key => $value) { ?>
                    <tr>
                        <td><?=$value['dateBooked'];?></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>

<?php } ?>