<?php
include('includes/bookingService.php');

$bookingService = new BookingService(true);

// $bookingService->printPre($communityList);
// $bookingService->printPre($_POST);
?>

<h1><?= $pageTitle; ?></h1>

<p>In this page you can make a new booking reservation.</p>
<p>The booking process is separated into three steps:</p>
<p>
    <ul>
        <li><b>Start Booking:</b> The booking is created and reserved in our system, so the booking is guaranteed to be yours for at least 15 minutes (usually this step occurs before getting into the payment step, for example);</li>
        <li><b>Make Booking:</b> The booking will be saved as "active" and VHC will be notified that the booking was finished and active;</li>
        <li><b>Cancel Booking:</b> If the Make Booking process wasn't executed yet, you can cancel your booking anytime using this. Let be clear that "active" bookings can only be cancelled by VHC;</li>
    </ul>
</p>

<form method="POST">
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                <label>Property ID <small><i>(Use <b>1_</b> at the start of the ID)</i></small></label>
                <input type="text" class="form-control" id="propertyid" name="propertyid" value="<?= $_POST['propertyid']; ?>">
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label>Date Checkin</label>
                <input type="date" class="form-control" id="checkin" name="checkin" value="<?= $_POST['checkin']; ?>">
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label>Date Checkout</label>
                <input type="date" class="form-control" id="checkout" name="checkout" value="<?= $_POST['checkout']; ?>">
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label>Language</label>
                <select class="form-control" id="language" name="language" value="<?= $_POST['language']; ?>">
                    <option <?= ($_POST['language'] == '0' ? 'selected' : ''); ?> value="0">English</option>
                    <option <?= ($_POST['language'] == '1' ? 'selected' : ''); ?> value="1">Portuguese</option>
                    <option <?= ($_POST['language'] == '2' ? 'selected' : ''); ?> value="2">Espanol</option>
                </select>
            </div>
            <div class="form-group">
                <label>Currency Wished</label>
                <select class="form-control" id="currency" name="currency" value="<?= $_POST['currency']; ?>">
                    <option <?= ($_POST['currency'] == 'USD' ? 'selected' : ''); ?> value="USD">USD</option>
                    <option <?= ($_POST['currency'] == 'BRL' ? 'selected' : ''); ?> value="BRL">BRL</option>
                </select>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>First Name</label>
                <input type="text" class="form-control" id="firstname" name="firstname" value="<?= $_POST['firstname']; ?>">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Last Name</label>
                <input type="text" class="form-control" id="lastname" name="lastname" value="<?= $_POST['lastname']; ?>">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Phone Number <small><i>(only numbers)</i></small></label>
                <input type="text" class="form-control" id="phone" name="phone" value="<?= $_POST['phone']; ?>">
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Email</label>
                <input type="text" class="form-control" id="email" name="email" value="<?= $_POST['email']; ?>">
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Message</label>
                <input type="text" class="form-control" id="message" name="message" value="<?= $_POST['message']; ?>">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Address</label>
                <input type="text" class="form-control" id="address" name="address" value="<?= $_POST['address']; ?>">
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label>Zip Code</label>
                <input type="text" class="form-control" id="zipcode" name="zipcode" value="<?= $_POST['zipcode']; ?>">
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label>City</label>
                <input type="text" class="form-control" id="city" name="city" value="<?= $_POST['city']; ?>">
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label>State</label>
                <input type="text" class="form-control" id="state" name="state" value="<?= $_POST['state']; ?>">
            </div>
        </div>
        <div class="col-lg-2">
            <div class="form-group">
                <label>Country</label>
                <input type="text" class="form-control" id="country" name="country" value="<?= $_POST['country']; ?>">
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label>Booking ID <small><i>(if applicable*)</i></small></label>
                <input type="text" class="form-control" id="bookingid" name="bookingid" value="<?= $_POST['bookingid']; ?>">
            </div>
        </div>
        <div class="col-lg-9 text-right">
            <button type="submit" class="btn btn-primary" name="action" value="start">Start Booking</button>
            <button type="submit" class="btn btn-success" name="action" value="make">Make Booking</button>
            <button type="submit" class="btn btn-danger" name="action" value="cancel">Cancel Booking</button>
        </div>
    </div>
</form>

<?php
if (!empty($_POST)) {
    if ($_POST['action'] == 'start') {
        $startBooking = $bookingService->startBooking($_POST);
        $bookingService->printPre($startBooking);
?>
        <br />

        <div class="row">
            <div class="col-lg-12">
                <?php if ($startBooking['bookingPlaced'] > 0) { ?>
                    <h4><b>[Start Booking]</b> Your booking is <span class=\"text-success\">created</span>! The booking number is <b><?= $startBooking['bookingID']; ?></b>, but is pending.<br />Use the ID for finish the booking process or manually cancel.</h4>
                <?php } else { ?>
                    <h4><b>[Start Booking]</b> Sorry. Your booking is <span class=\"text-danger\">not created</span>. Please review the availability and the info and try again.</h4>
                <?php } ?>
            </div>
        </div>

    <?php   } else if ($_POST['action'] == 'make') {
        $makeBooking = $bookingService->makeBooking($_POST);
        $errorIfExists = $bookingService->getErrorFromResult($makeBooking);
    ?>
        <br />

        <div class="row">
            <div class="col-lg-12">
                <?php if (!empty($errorIfExists)) { ?>
                    <h4><b>[Cancel Booking]</b> ERROR: <?= $errorIfExists; ?></h4>
                <?php } else { ?>
                    <?php if ($makeBooking['bookingPlaced'] > 0) { ?>
                        <h4><b>[Make Booking]</b> Your booking is <span class=\"text-success\">confirmed</span>! Your booking number is <b><?= $makeBooking['bookingID']; ?></b>!</h4>
                    <?php } else { ?>
                        <h4><b>[Make Booking]</b> Sorry. Your booking is <span class=\"text-danger\">not confirmed</span>. Please review the availability and the info and try again.</h4>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>

    <?php   } else if ($_POST['action'] == 'cancel') {
        $cancelBooking = $bookingService->cancelPendingBooking($_POST);
        $errorIfExists = $bookingService->getErrorFromResult($cancelBooking);
    ?>
        <br />

        <div class="row">
            <div class="col-lg-12">
                <?php if (!empty($errorIfExists)) { ?>
                    <h4><b>[Cancel Booking]</b> ERROR: <?= $errorIfExists; ?></h4>
                <?php } else { ?>
                    <h4><b>[Cancel Booking]</b> Action result: <?= $cancelBooking['bookingCancelResults'][key($cancelBooking['bookingCancelResults'])]; ?><br />Booking number: <b><?= $_POST['bookingid']; ?></b></h4>
                <?php } ?>
            </div>
        </div>
<?php }
} ?>