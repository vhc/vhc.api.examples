<?php
include('includes/propertyService.php');

$propertyService = new PropertyService();

$apiToken = $propertyService->doLogin()['apiToken'];
$amenityList = $propertyService->amenityList($apiToken);
$categoryList = $propertyService->categoryList($apiToken);
$classList = $propertyService->classList($apiToken);
$communityList = $propertyService->communityList($apiToken);
$typeList = $propertyService->typeList($apiToken);
$zoneList = $propertyService->zoneList($apiToken);

// $propertyService->printPre($communityList);
// $propertyService->printPre($_POST);
?>

<h1><?= $pageTitle; ?></h1>

<p>In this page you can search for properties using many filters available.</p>

<form method="POST">
    <div class="row">
        <div class="col-lg-4">
            <div class="form-group">
                <label>Property ID <small><i>(Use 1_ at the start of the ID. Ex.: 1_123456)</i></small></label>
                <input type="text" class="form-control" id="propertyid" name="propertyid" value="<?= $_POST['propertyid']; ?>">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Date From</label>
                <input type="date" class="form-control" id="checkin" name="checkin" value="<?= $_POST['checkin']; ?>">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Date To</label>
                <input type="date" class="form-control" id="checkout" name="checkout" value="<?= $_POST['checkout']; ?>">
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Zones</label>
                <select multiple class="form-control" id="zones" name="zones[]">
                    <?php foreach ($zoneList as $key => $value) {
                        $selected = (in_array($value['id'], $_POST['zones']) ? 'selected' : ''); ?>
                        <option <?= $selected; ?> value="<?= $value['id']; ?>"><?= $value['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Communities</label>
                <select multiple class="form-control" id="communities" name="communities[]">
                    <?php
                    foreach ($communityList as $keyCm => $valueCm) {
                        foreach ($valueCm['communities'] as $key => $value) {
                            $selected = (in_array($value, $_POST['communities']) ? 'selected' : ''); ?>
                            <option <?= $selected; ?> value="<?= $value; ?>"><?= $value; ?></option>
                    <?php }
                    } ?>
                </select>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Categories</label>
                <select multiple class="form-control" id="categories" name="categories[]">
                    <?php foreach ($categoryList as $key => $value) {
                        $selected = (in_array($value['id'], $_POST['categories']) ? 'selected' : ''); ?>
                        <option <?= $selected; ?> value="<?= $value['id']; ?>"><?= $value['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Types</label>
                <select multiple class="form-control" id="types" name="types[]">
                    <?php foreach ($typeList as $key => $value) {
                        $selected = (in_array($value, $_POST['types']) ? 'selected' : ''); ?>
                        <option <?= $selected; ?> value="<?= $value; ?>"><?= $value; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Amenities</label>
                <select multiple class="form-control" id="amenities" name="amenities[]">
                    <?php foreach ($amenityList as $key => $value) {
                        $selected = (in_array($value['id'], $_POST['amenities']) ? 'selected' : ''); ?>
                        <option <?= $selected; ?> value="<?= $value['id']; ?>"><?= $value['name']; ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="form-group">
                <label>Language</label>
                <select class="form-control" id="language" name="language" value="<?= $_POST['language']; ?>">
                    <option <?= ($_POST['language'] == '0' ? 'selected' : ''); ?> value="0">English</option>
                    <option <?= ($_POST['language'] == '1' ? 'selected' : ''); ?> value="1">Portuguese</option>
                    <option <?= ($_POST['language'] == '2' ? 'selected' : ''); ?> value="2">Espanol</option>
                </select>
            </div>
            <div class="form-group">
                <label>Currency Wished</label>
                <select class="form-control" id="currency" name="currency" value="<?= $_POST['currency']; ?>">
                    <option <?= ($_POST['currency'] == 'USD' ? 'selected' : ''); ?> value="USD">USD</option>
                    <option <?= ($_POST['currency'] == 'BRL' ? 'selected' : ''); ?> value="BRL">BRL</option>
                </select>
            </div>
        </div>
        <div class="col-lg-12 text-right">
            <button type="submit" class="btn btn-primary">Search</button>
        </div>
    </div>
</form>

<br />

<?php if (!empty($_POST)) {
    $searchProperties = $propertyService->list(
        $_POST['propertyid'],
        $_POST['checkin'],
        $_POST['checkout'],
        $_POST['zones'],
        $_POST['communities'],
        $_POST['categories'],
        $_POST['types'],
        $_POST['amenities'],
        $_POST['language'],
        $_POST['currency']
    ); ?>
    <div class="row">
        <div class="col-lg-12">
            <table class="table table-responsive-lg text-center">
                <thead>
                    <tr>
                        <th>Property ID</th>
                        <th>Property Code</th>
                        <th>Community</th>
                        <th>Price Per Night (Base)</th>
                        <th>Price Calculated (if applicable)</th>
                        <th style="width: 25%;">Property Image</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($searchProperties as $key => $value) { ?>
                        <tr>
                            <td><?= $value['idPmsExternal']; ?></td>
                            <td><?= $value['mcPropertyName']; ?></td>
                            <td><?= $value['name']; ?></td>
                            <td>$ <?= number_format((float) $value['pricePerNightBase'], 2, '.', ''); ?></td>
                            <td><?= (!empty($value['price']) ? 'Total $ '.number_format((float) $value['price']['total'], 2, '.', '').' | '.$value['price']['numberOfNights'].' night(s)' : ''); ?></td>
                            <td><img src="<?=$value['imageSource'];?>" class="img-fluid img-thumbnail" /></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
<? } ?>
