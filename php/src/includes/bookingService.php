<?php
include('baseService.php');

class BookingService extends BaseService {
    function __construct($isDebug = false) {
        parent::__construct($isDebug);
    }

    function startBooking($postResult) {
        $body = new Booking($postResult);
        print_r(json_encode($body));

        $apiToken = $this->doLogin()['apiToken'];
        return $this->executeRestAPI('POST', 'api/Booking/StartBooking', $apiToken, null, $body);
    }

    function makeBooking($postResult)
    {
        $body = new Booking($postResult);

        $apiToken = $this->doLogin()['apiToken'];
        return $this->executeRestAPI('POST', 'api/Booking/MakeBooking', $apiToken, null, $body);
    }

    function cancelPendingBooking($postResult)
    {
        $body = [ 'bookingId' => $postResult['bookingid'] ];

        $apiToken = $this->doLogin()['apiToken'];
        return $this->executeRestAPI('POST', 'api/Booking/CancelPendingBooking', $apiToken, null, $body);
    }
}

class Booking {
    public $propertyid;
    public $checkin;
    public $checkout;
    public $language;
    public $currencywished;
    public $firstname;
    public $lastname;
    public $phone;
    public $email;
    public $message;
    public $address;
    public $zipcode;
    public $city;
    public $state;
    public $country;
    public $bookingid;

    function __construct($post = null) {
        if($post != null) {
            $this->convertFromPost($post);
        }
    }

    function convertFromPost($post) {
        $this->propertyid = $post['propertyid'];
        $this->checkin = $post['checkin'];
        $this->checkout = $post['checkout'];
        $this->language = $post['language'];
        $this->currencywished = $post['currency'];
        $this->firstname = $post['firstname'];
        $this->lastname = $post['lastname'];
        $this->phone = $post['phone'];
        $this->email = $post['email'];
        $this->message = $post['message'];
        $this->address = $post['address'];
        $this->zipcode = $post['zipcode'];
        $this->city = $post['city'];
        $this->state = $post['state'];
        $this->country = $post['country'];
        $this->bookingid = $post['bookingid'];
    }
}
?>