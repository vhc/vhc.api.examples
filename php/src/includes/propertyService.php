<?php
include('baseService.php');

class PropertyService extends BaseService {
    function __construct($isDebug = false) {
        parent::__construct($isDebug);
    }

    function isPropertyAvailable($propertyId, $checkinDate, $checkoutDate) {
        $apiToken = $this->doLogin()['apiToken'];
        $parameters = [ 'checkinDate' => $checkinDate, 'checkoutDate' => $checkoutDate ];
        return $this->executeRestAPI('GET', 'api/Property/IsAvailable/'.$propertyId, $apiToken, $parameters);
    }

    function list($propertyId, $checkin, $checkout, $zones, $communities, $categories, $types, $amenities, $language, $currency) {
        $body = [
            'idProperty' => $propertyId,
            'dateFrom' => $checkin,
            'dateTo' => $checkout,
            'priceFrom' => null,
            'priceTo' => null,
            'zones' => $zones,
            'communities' => $communities,
            'categories' => $categories,
            'types' => $types,
            'amenities' => $amenities,
            'bedrooms' => null,
            'guest' => null,
            'language' => $language,
            'currencyWished' => $currency,
        ];

        $apiToken = $this->doLogin()['apiToken'];
        return $this->executeRestAPI('POST', 'api/Property/List', $apiToken, null, $body);
    }

    function get($propertyId, $currency, $language) {
        $body = [
            'id' => $propertyId,
            'currencyWished' => $currency,
            'language' => $language,
            'withCache' => true
        ];
        
        $apiToken = $this->doLogin()['apiToken'];
        return $this->executeRestAPI('POST', 'api/Property/Get', $apiToken, null, $body);
    }

    function getReservedDates($propertyId) {
        $apiToken = $this->doLogin()['apiToken'];
        return $this->executeRestAPI('GET', 'api/Property/GetReservedDatesByProperty/'.$propertyId, $apiToken);
    }

    function amenityList(&$apiToken = null) {
        if(empty($apiToken)) $apiToken = $this->doLogin()['apiToken'];
        $parameters = [ 'Language' => Language::ENGLISH ];
        return $this->executeRestAPI('GET', 'api/Amenity/List', $apiToken, $parameters);
    }

    function categoryList(&$apiToken = null) {
        if(empty($apiToken)) $apiToken = $this->doLogin()['apiToken'];
        $parameters = [ 'Language' => Language::ENGLISH ];
        return $this->executeRestAPI('GET', 'api/Category/List', $apiToken, $parameters);
    }

    function classList(&$apiToken = null) {
        if(empty($apiToken)) $apiToken = $this->doLogin()['apiToken'];
        return $this->executeRestAPI('GET', 'api/Class/List', $apiToken);
    }

    function communityList(&$apiToken = null) {
        if(empty($apiToken)) $apiToken = $this->doLogin()['apiToken'];
        $parameters = [ 'Language' => Language::ENGLISH ];
        return $this->executeRestAPI('GET', 'api/Community/List', $apiToken, $parameters);
    }

    function typeList(&$apiToken = null) {
        if(empty($apiToken)) $apiToken = $this->doLogin()['apiToken'];
        return $this->executeRestAPI('GET', 'api/Type/List', $apiToken);
    }

    function zoneList(&$apiToken = null) {
        if(empty($apiToken)) $apiToken = $this->doLogin()['apiToken'];
        $parameters = [ 'Language' => Language::ENGLISH ];
        return $this->executeRestAPI('GET', 'api/Zone/List', $apiToken, $parameters);
    }
}
?>