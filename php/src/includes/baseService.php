<?php
include('config.php');
include('vendor/autoload.php');

class BaseService {
    public string $Token = '';
    public string $Url = '';
    
    private bool $Debug = false;
    private string $Login = '';
    private string $Password = '';

    function __construct($isDebug = false) {
        $config = $GLOBALS['config'];

        $this->Url = $config['url'];
        $this->Login = $config['login'];
        $this->Password = $config['password'];

        $this->Debug = $isDebug;
    }

    function printPre($obj) {
        echo '<pre>';
        print_r($obj);
        echo '</pre>';
    }

    function doLogin() {
        $requestBody = [
            'userName' => $this->Login,
            'password' => $this->Password,
            'authenticationFrom' => 1
        ];

        $client = new GuzzleHttp\Client(['base_uri' => $this->Url]);
        $response = $client->request('POST', 'api/Login', [
            'headers' => ['Content-Type' => 'application/json', 'Accept' => 'application/json'],
            'body' => json_encode($requestBody)
        ], );

        $body = json_decode($response->getBody(), true);
        return $body;
    }

    function getErrorFromResult($body) {
        return (!empty($body['exception']['error'][0]) ? $body['exception']['error'][0] : null);
    }

    function executeRestAPI($methodType, $url, $apiToken, $parameters = null, $body = null) {
        $headers = [ 'Content-Type' => 'application/json', 'Accept' => 'application/json' ];
        
        if(!empty($apiToken)) {
            $headers['Authorization'] = 'Bearer '.$apiToken;
        }

        $strUrl = $url;
        $strBody = (!empty($body) ? json_encode($body) : null);
        if(!empty($parameters)) {
            $idx = 0;
            foreach($parameters as $parKey => $parValue) {
                if($idx === 0) {
                    $strUrl .= '?';
                } else {
                    $strUrl .= '&';
                }
                $strUrl .= $parKey.'='.$parValue;
                $idx++;
            }
        }

        try {
            $client = new GuzzleHttp\Client(['base_uri' => $this->Url]);
            $response = $client->request($methodType, $strUrl, [
                'headers' => $headers,
                'body' => $strBody
            ]);

            $body = json_decode($response->getBody(), true);
        } catch (GuzzleHttp\Exception\ServerException $e) {
            $responseException = $e->getResponse();
            $body = json_decode($responseException->getBody()->getContents(), true);
        } catch (GuzzleHttp\Exception\ClientException $e) {
            $responseException = $e->getResponse();
            $body = json_decode($responseException->getBody()->getContents(), true);
        } catch (GuzzleHttp\Exception\ClientErrorResponseException $exception) {
            $body = json_decode($exception->getResponse()->getBody(true), true);
        }
        
        if($this->Debug) print_r('<b>[DEBUG] URL Executed: </b>'.$this->Url.$strUrl.'<br />');
        return $body;
    }
}

?>