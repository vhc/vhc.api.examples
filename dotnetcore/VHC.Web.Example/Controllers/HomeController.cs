﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using VHC.Web.Example.Models;
using VHC.Web.Example.Services;

namespace VHC.Web.Example.Controllers
{
    public class HomeController : Controller
    {
        private PropertyServices PropertyService { get; set; }
        private BookingServices BookingService { get; set; }
        public HomeController(IConfiguration configuration)
        {
            this.PropertyService = new PropertyServices(configuration);
            this.BookingService = new BookingServices(configuration);
        }

        public IActionResult Index()
        {
            ViewData["Title"] = "Home";
            return View();
        }

        public IActionResult Properties()
        {
            ViewData["Title"] = "Property List";
            this.SetViewBags();

            return View();
        }

        [HttpPost]
        public IActionResult Properties(PropertyListModel model)
        {
            ViewData["Title"] = "Property List";
            this.SetViewBags();

            var result = this.PropertyService.List(model);
            return View(result);
        }

        public IActionResult Availability()
        {
            ViewData["Title"] = "Property Availability";
            return View();
        }

        [HttpPost]
        public IActionResult Availability(IsPropertyAvailableModel model)
        {
            ViewData["Title"] = "Property Availability";

            var resultAvailableToBook = this.PropertyService.IsPropertyAvailable(model);
            //var resultAvailableToBook = false;

            ViewBag.DateAvailabilityCheck = new AvailabilityCheckModel
            {
                Available = resultAvailableToBook,
                IdProperty = model.Id,
                Checkin = model.CheckinDate,
                Checkout = model.CheckoutDate
            };

            var result = this.PropertyService.GetReservedDates(model);

            return View(result);
        }

        public IActionResult Booking()
        {
            ViewData["Title"] = "Booking";
            return View();
        }

        [HttpPost]
        public IActionResult Booking(Booking model, string action)
        {
            ViewData["Title"] = "Booking";

            Booking makeBooking = null;
            Booking resultBooking = null;
            BookingCancelModel cancelBooking = null;

            switch (action)
            {
                case "start":
                    resultBooking = this.BookingService.StartBooking(model);
                    ViewBag.StartBookingResult = resultBooking;
                    model.BookingId = resultBooking.BookingId;
                    model.BookingPlaced = resultBooking.BookingPlaced;
                    break;
                case "make":
                    makeBooking = this.BookingService.MakeBooking(model);
                    ViewBag.MakeBookingResult = makeBooking;
                    model.BookingPlaced = makeBooking.BookingPlaced;
                    break;
                case "cancel":
                    cancelBooking = this.BookingService.CancelPendingBooking(model);
                    ViewBag.CancelBookingResult = cancelBooking;
                    break;
            }

            return View(model);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public void SetViewBags()
        {
            string apiToken = null;
            ViewBag.Communities = this.PropertyService.CommunityList(ref apiToken).SelectMany(s => s.Communities).ToList();
            ViewBag.Categories = this.PropertyService.CategoryList(ref apiToken);
            ViewBag.Types = this.PropertyService.TypeList(ref apiToken);
            ViewBag.Amenities = this.PropertyService.AmenitiesList(ref apiToken);
            ViewBag.Zones = this.PropertyService.ZoneList(ref apiToken);
        }
    }
}
