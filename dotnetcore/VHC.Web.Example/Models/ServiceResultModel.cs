﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public class ServiceResultModel
    {
        [JsonProperty("idService")]
        public string IdService { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("priceUnit")]
        public int PriceUnit { get; set; }

        [JsonProperty("taxFeeUnit")]
        public int TaxFeeUnit { get; set; }

        [JsonProperty("dailyFee")]
        public bool DailyFee { get; set; }

        [JsonProperty("flatFee")]
        public bool FlatFee { get; set; }

        [JsonProperty("chargeTaxs")]
        public ChargeTax ChargeTaxs { get; set; }

        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("totalTaxFee")]
        public int TotalTaxFee { get; set; }

        [JsonProperty("ptax")]
        public int Ptax { get; set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }
    }
}
