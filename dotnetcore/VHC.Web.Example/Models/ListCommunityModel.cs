﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public class ListCommunityModel
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("communities")]
        public List<string> Communities { get; set; }
    }
}
