﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public class PropertyListResultModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("idPmsExternal")]
        public string IdPmsExternal { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("imageSource")]
        public string ImageSource { get; set; }

        [JsonProperty("mcPropertyName")]
        public string McPropertyName { get; set; }

        [JsonProperty("class")]
        public string Class { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("numberRooms")]
        public int NumberRooms { get; set; }

        [JsonProperty("bathRooms")]
        public decimal BathRooms { get; set; }

        [JsonProperty("numberGuests")]
        public int NumberGuests { get; set; }

        [JsonProperty("rateStars")]
        public int RateStars { get; set; }

        [JsonProperty("reviews")]
        public int Reviews { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("statusDescription")]
        public string StatusDescription { get; set; }

        [JsonProperty("pricePerNightBase")]
        public decimal PricePerNightBase { get; set; }

        [JsonProperty("price")]
        public Price Price { get; set; }
    }

    public partial class Price
    {
        [JsonProperty("checkin")]
        public DateTime Checkin { get; set; }

        [JsonProperty("checkout")]
        public DateTime Checkout { get; set; }

        [JsonProperty("checkin00")]
        public DateTime Checkin00 { get; set; }

        [JsonProperty("checkout00")]
        public DateTime Checkout00 { get; set; }

        [JsonProperty("numberOfNightsPricesIncluded")]
        public decimal NumberOfNightsPricesIncluded { get; set; }

        [JsonProperty("numberOfNights")]
        public int NumberOfNights { get; set; }

        [JsonProperty("pricePerNightCalculated")]
        public decimal PricePerNightCalculated { get; set; }

        [JsonProperty("allNightsHasPricesIncluded")]
        public bool AllNightsHasPricesIncluded { get; set; }

        [JsonProperty("rentalRate")]
        public decimal RentalRate { get; set; }

        [JsonProperty("cleaningFee")]
        public decimal CleaningFee { get; set; }

        [JsonProperty("servicesMandatory")]
        public List<ServicesMandatory> ServicesMandatory { get; set; }

        [JsonProperty("servicesValue")]
        public decimal ServicesValue { get; set; }

        [JsonProperty("bookingFee")]
        public decimal BookingFee { get; set; }

        [JsonProperty("subtotal")]
        public decimal Subtotal { get; set; }

        [JsonProperty("taxFee")]
        public decimal TaxFee { get; set; }

        [JsonProperty("total")]
        public decimal Total { get; set; }

        [JsonProperty("taxPercents")]
        public TaxPercents TaxPercents { get; set; }

        [JsonProperty("chargeTaxOnRentals")]
        public ChargeTax ChargeTaxOnRentals { get; set; }

        [JsonProperty("chargeTaxOnPoolHeats")]
        public ChargeTax ChargeTaxOnPoolHeats { get; set; }

        [JsonProperty("chargeTaxOnBookingFees")]
        public ChargeTax ChargeTaxOnBookingFees { get; set; }

        [JsonProperty("chargeTaxOnCleaningFees")]
        public ChargeTax ChargeTaxOnCleaningFees { get; set; }

        [JsonProperty("currencyDefault")]
        public string CurrencyDefault { get; set; }

        [JsonProperty("currencyWished")]
        public string CurrencyWished { get; set; }

        [JsonProperty("ptax")]
        public decimal Ptax { get; set; }
    }

    public partial class ChargeTax
    {
        [JsonProperty("additionalProp1")]
        public bool AdditionalProp1 { get; set; }

        [JsonProperty("additionalProp2")]
        public bool AdditionalProp2 { get; set; }

        [JsonProperty("additionalProp3")]
        public bool AdditionalProp3 { get; set; }
    }

    public partial class ServicesMandatory
    {
        [JsonProperty("idService")]
        public string IdService { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("priceUnit")]
        public decimal PriceUnit { get; set; }

        [JsonProperty("taxFeeUnit")]
        public decimal TaxFeeUnit { get; set; }

        [JsonProperty("dailyFee")]
        public bool DailyFee { get; set; }

        [JsonProperty("flatFee")]
        public bool FlatFee { get; set; }

        [JsonProperty("chargeTaxs")]
        public ChargeTax ChargeTaxs { get; set; }

        [JsonProperty("total")]
        public decimal Total { get; set; }

        [JsonProperty("totalTaxFee")]
        public decimal TotalTaxFee { get; set; }

        [JsonProperty("ptax")]
        public decimal Ptax { get; set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }
    }

    public partial class TaxPercents
    {
        [JsonProperty("additionalProp1")]
        public decimal AdditionalProp1 { get; set; }

        [JsonProperty("additionalProp2")]
        public decimal AdditionalProp2 { get; set; }

        [JsonProperty("additionalProp3")]
        public decimal AdditionalProp3 { get; set; }
    }
}
