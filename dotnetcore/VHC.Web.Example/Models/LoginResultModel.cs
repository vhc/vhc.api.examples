﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public class LoginResultModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("pmsInformation")]
        public string PmsInformation { get; set; }

        [JsonProperty("apiToken")]
        public string ApiToken { get; set; }

        [JsonProperty("apiTokenCreated")]
        public DateTime ApiTokenCreated { get; set; }

        [JsonProperty("apiTokenExpireOn")]
        public DateTime ApiTokenExpireOn { get; set; }

        [JsonProperty("userName")]
        public string UserName { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }
    }
}
