﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public class AvailabilityCheckModel
    {
        [JsonProperty("id")]
        public string IdProperty { get; set; }

        [JsonProperty("available")]
        public bool Available { get; set; }

        [JsonProperty("checkin")]
        public DateTime Checkin { get; set; }

        [JsonProperty("checkout")]
        public DateTime Checkout { get; set; }
    }
}
