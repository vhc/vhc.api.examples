﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public class IsPropertyAvailableModel
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("checkinDate")]
        public DateTime CheckinDate { get; set; }

        [JsonProperty("checkoutDate")]
        public DateTime CheckoutDate { get; set; }
    }
}
