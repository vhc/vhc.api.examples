﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public class PropertyGetResultModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("idCompany")]
        public int IdCompany { get; set; }

        [JsonProperty("idPmsExternal")]
        public string IdPmsExternal { get; set; }

        [JsonProperty("propertyName")]
        public string PropertyName { get; set; }

        [JsonProperty("propertyDescription")]
        public string PropertyDescription { get; set; }

        [JsonProperty("propertyClass")]
        public string PropertyClass { get; set; }

        [JsonProperty("propertyType")]
        public string PropertyType { get; set; }

        [JsonProperty("priceBase")]
        public decimal PriceBase { get; set; }

        [JsonProperty("zones")]
        public List<string> Zones { get; set; }

        [JsonProperty("propertyCategories")]
        public List<PropertyCategory> PropertyCategories { get; set; }

        [JsonProperty("community")]
        public Community Community { get; set; }

        [JsonProperty("featuredPicture")]
        public FeaturedPicture FeaturedPicture { get; set; }

        [JsonProperty("housePictures")]
        public List<FeaturedPicture> HousePictures { get; set; }

        [JsonProperty("housePictures3D")]
        public List<FeaturedPicture> HousePictures3D { get; set; }

        [JsonProperty("bathrooms")]
        public decimal Bathrooms { get; set; }

        [JsonProperty("bedrooms")]
        public int Bedrooms { get; set; }

        [JsonProperty("guests")]
        public int Guests { get; set; }

        [JsonProperty("latitude")]
        public int Latitude { get; set; }

        [JsonProperty("longitude")]
        public int Longitude { get; set; }

        [JsonProperty("propertyAddress")]
        public string PropertyAddress { get; set; }

        [JsonProperty("propertyAddress2")]
        public string PropertyAddress2 { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("telephone")]
        public string Telephone { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }

        [JsonProperty("amenities")]
        public List<Amenity> Amenities { get; set; }

        [JsonProperty("amenitiesNotIncluded")]
        public List<Amenity> AmenitiesNotIncluded { get; set; }

        [JsonProperty("attractions")]
        public List<Attraction> Attractions { get; set; }

        [JsonProperty("relatedProperties")]
        public List<RelatedProperty> RelatedProperties { get; set; }

        [JsonProperty("price")]
        public Price Price { get; set; }

        [JsonProperty("isAvailable")]
        public bool IsAvailable { get; set; }

        [JsonProperty("reservedDates")]
        public List<ReservedDate> ReservedDates { get; set; }

        [JsonProperty("stars")]
        public int Stars { get; set; }

        [JsonProperty("starsEvaluatedBy")]
        public long StarsEvaluatedBy { get; set; }

        [JsonProperty("status")]
        public long Status { get; set; }

        [JsonProperty("statusDescription")]
        public string StatusDescription { get; set; }
    }

    public partial class Amenity
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("urlIcon")]
        public string UrlIcon { get; set; }
    }

    public partial class Attraction
    {
        [JsonProperty("id")]
        public long Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("latitude")]
        public long Latitude { get; set; }

        [JsonProperty("longitude")]
        public long Longitude { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("distanceInMeters")]
        public long DistanceInMeters { get; set; }

        [JsonProperty("distanceInKm")]
        public long DistanceInKm { get; set; }

        [JsonProperty("distanceInMiles")]
        public long DistanceInMiles { get; set; }

        [JsonProperty("featuredPicture")]
        public FeaturedPicture FeaturedPicture { get; set; }

        [JsonProperty("iconPicture")]
        public FeaturedPicture IconPicture { get; set; }
    }

    public partial class FeaturedPicture
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("urlImageThumbnail")]
        public string UrlImageThumbnail { get; set; }

        [JsonProperty("urlImageRegular")]
        public string UrlImageRegular { get; set; }

        [JsonProperty("urlImageLarge")]
        public string UrlImageLarge { get; set; }

        [JsonProperty("urlImageOriginal")]
        public string UrlImageOriginal { get; set; }

        [JsonProperty("caption")]
        public string Caption { get; set; }

        [JsonProperty("urlLink")]
        public string UrlLink { get; set; }

        [JsonProperty("status")]
        public bool Status { get; set; }
    }

    public partial class Community
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("featuredPicture")]
        public List<FeaturedPicture> FeaturedPicture { get; set; }

        [JsonProperty("youtubeVideo")]
        public string YoutubeVideo { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("latitude")]
        public int Latitude { get; set; }

        [JsonProperty("longitude")]
        public int Longitude { get; set; }
    }

    public partial class PropertyCategory
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("idCompany")]
        public int IdCompany { get; set; }

        [JsonProperty("idPicture")]
        public int IdPicture { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("created")]
        public DateTime Created { get; set; }

        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }
    }

    public partial class RelatedProperty
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("idPmsExternal")]
        public string IdPmsExternal { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("imageSource")]
        public string ImageSource { get; set; }

        [JsonProperty("mcPropertyName")]
        public string McPropertyName { get; set; }

        [JsonProperty("class")]
        public string Class { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("numberRooms")]
        public int NumberRooms { get; set; }

        [JsonProperty("bathRooms")]
        public decimal BathRooms { get; set; }

        [JsonProperty("numberGuests")]
        public int NumberGuests { get; set; }

        [JsonProperty("rateStars")]
        public int RateStars { get; set; }

        [JsonProperty("reviews")]
        public int Reviews { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("statusDescription")]
        public string StatusDescription { get; set; }

        [JsonProperty("pricePerNightBase")]
        public decimal PricePerNightBase { get; set; }

        [JsonProperty("price")]
        public Price Price { get; set; }
    }

    public partial class ReservedDate
    {
        [JsonProperty("dateBooked")]
        public DateTime DateBooked { get; set; }
    }

    public partial class DateAvailable
    {
        [JsonProperty("isAvailable")]
        public bool IsAvailable { get; set; }
    }
}
