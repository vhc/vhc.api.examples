﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public class Enums
    {
        public enum Language
        {
            English = 0,
            Portuguese = 1,
            Espanol = 2
        }

        public enum CurrencyWished
        {
            USD = 0,
            BRL = 1
        }
    }
}
