﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public class PropertyListModel
    {
        [JsonProperty("idProperty")]
        public string IdProperty { get; set; }

        [JsonProperty("dateFrom")]
        public DateTime? DateFrom { get; set; }

        [JsonProperty("dateTo")]
        public DateTime? DateTo { get; set; }

        [JsonProperty("priceFrom")]
        public int? PriceFrom { get; set; }

        [JsonProperty("priceTo")]
        public int? PriceTo { get; set; }

        [JsonProperty("zones")]
        public List<int> Zones { get; set; }

        [JsonProperty("communities")]
        public List<string> Communities { get; set; }

        [JsonProperty("categories")]
        public List<string> Categories { get; set; }

        [JsonProperty("types")]
        public List<string> Types { get; set; }

        [JsonProperty("amenities")]
        public List<string> Amenities { get; set; }

        [JsonProperty("bedRooms")]
        public int? BedRooms { get; set; }

        [JsonProperty("guest")]
        public int? Guest { get; set; }

        [JsonProperty("language")]
        public int Language { get; set; }

        [JsonProperty("currencyWished")]
        public string CurrencyWished { get; set; }
    }
}
