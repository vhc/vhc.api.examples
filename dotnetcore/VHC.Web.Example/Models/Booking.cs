﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public partial class Booking
    {
        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("bookingID")]
        public string BookingId { get; set; }

        [JsonProperty("bookingPlaced")]
        public bool BookingPlaced { get; set; }

        [JsonProperty("calculatedPrice")]
        public decimal CalculatedPrice { get; set; }

        [JsonProperty("checkin")]
        public DateTime Checkin { get; set; }

        [JsonProperty("checkout")]
        public DateTime Checkout { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("currencyWished")]
        public string CurrencyWished { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("firstName")]
        public string FirstName { get; set; }

        [JsonProperty("language")]
        public int Language { get; set; }

        [JsonProperty("lastName")]
        public string LastName { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("propertyID")]
        public string PropertyId { get; set; }

        [JsonProperty("servicesMandatory")]
        public List<Services> ServicesMandatory { get; set; }

        [JsonProperty("servicesOptional")]
        public List<Services> ServicesOptional { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("zipCode")]
        public string ZipCode { get; set; }
    }

    public partial class Services
    {
        [JsonProperty("dailyFee")]
        public bool DailyFee { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("flatFee")]
        public bool FlatFee { get; set; }

        [JsonProperty("idService")]
        public string IdService { get; set; }

        [JsonProperty("imageUrl")]
        public string ImageUrl { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("priceUnit")]
        public decimal PriceUnit { get; set; }

        [JsonProperty("ptax")]
        public decimal Ptax { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("taxFeeUnit")]
        public decimal TaxFeeUnit { get; set; }
    }
}
