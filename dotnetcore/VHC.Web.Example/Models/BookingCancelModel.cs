﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VHC.Web.Example.Models
{
    public partial class BookingCancelModel
    {
        [JsonProperty("bookingID")]
        public string BookingId { get; set; }

        [JsonProperty("bookingCancelResults")]
        public Dictionary<string, string> BookingCancelResults { get; set; }
    }

}
