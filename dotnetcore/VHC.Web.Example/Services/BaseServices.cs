﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VHC.Web.Example.Models;

namespace VHC.Web.Example.Services
{
    public class BaseServices
    {
        public string Token { get; set; }
        public string Url { get; set; }
        public IConfiguration Configuration { get; }

        public BaseServices(IConfiguration configuration)
        {
            this.Configuration = configuration;
            this.Url = Configuration["Authentication:URL"];
        }

        public string DoLogin()
        {
            var loginResult = this.Login();
            this.Token = loginResult.ApiToken;
            return this.Token;
        }

        public LoginResultModel Login()
        {
            string login = Configuration["Authentication:Login"];
            string password = Configuration["Authentication:Password"];

            var client = new RestClient(this.Url);
            //client.Authenticator = new JwtAuthenticator(token);

            var request = new RestRequest("api/Login", Method.POST, DataFormat.Json);
            request.AddJsonBody(new LoginModel
            {
                UserName = login,
                Password = password
            });

            var response = client.Execute(request);

            return JsonConvert.DeserializeObject<LoginResultModel>(response.Content);
        }

        public T ExecuteRestAPI<T>(Method urlMethod, string url, string apiToken, Dictionary<string, object> parameters = null, object body = null)
        {
            var client = new RestClient(this.Url);

            if(!string.IsNullOrEmpty(apiToken)) 
                client.Authenticator = new JwtAuthenticator(apiToken);

            var request = new RestRequest(url, urlMethod, DataFormat.Json);

            if(parameters != null)
                foreach(var item in parameters)
                    request.AddParameter(item.Key, item.Value);

            if (body != null)
                request.AddJsonBody(body);

            var response = client.Execute(request);

            if (!response.IsSuccessful) throw new Exception("REST API -> Request failed", (response.ErrorException != null ? response.ErrorException : new Exception(response.Content)));
            return JsonConvert.DeserializeObject<T>(response.Content);
        }
    }
}
