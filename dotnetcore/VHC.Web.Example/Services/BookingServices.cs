﻿using Microsoft.Extensions.Configuration;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VHC.Web.Example.Models;

namespace VHC.Web.Example.Services
{
    public class BookingServices : BaseServices
    {
        public BookingServices(IConfiguration configuration) : base(configuration)
        {

        }

        public Booking StartBooking(Booking model)
        {
            string apiToken = this.DoLogin();
            return this.ExecuteRestAPI<Booking>(Method.POST, "api/Booking/StartBooking", apiToken, null, model);
        }

        public Booking MakeBooking(Booking model)
        {
            string apiToken = this.DoLogin();
            return this.ExecuteRestAPI<Booking>(Method.POST, "api/Booking/MakeBooking", apiToken, null, model);
        }

        public BookingCancelModel CancelPendingBooking(Booking model)
        {
            string apiToken = this.DoLogin();

            var bookingCancel = new BookingCancelModel
            {
                BookingId = model.BookingId
            };

            return this.ExecuteRestAPI<BookingCancelModel>(Method.POST, "api/Booking/CancelPendingBooking", apiToken, null, bookingCancel);
        }
    }
}
