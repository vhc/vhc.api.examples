﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VHC.Web.Example.Models;

namespace VHC.Web.Example.Services
{
    public class PropertyServices : BaseServices
    {
        public PropertyServices(IConfiguration configuration) : base(configuration) 
        { 
        
        }

        public List<PropertyListResultModel> List(PropertyListModel model)
        {
            string apiToken = this.DoLogin();
            return this.ExecuteRestAPI<List<PropertyListResultModel>>(Method.POST, "api/Property/List", apiToken, null, model);
        }

        public PropertyGetResultModel Get(PropertyGetModel model)
        {
            string apiToken = this.DoLogin();
            return this.ExecuteRestAPI<PropertyGetResultModel>(Method.POST, "api/Property/Get", apiToken, null, model);
        }

        public bool IsPropertyAvailable(IsPropertyAvailableModel model)
        {
            string apiToken = this.DoLogin();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("checkinDate", model.CheckinDate.Date.ToString("yyyy-MM-dd"));
            parameters.Add("checkoutDate", model.CheckoutDate.Date.ToString("yyyy-MM-dd"));

            var result = this.ExecuteRestAPI<DateAvailable>(Method.GET, $"api/Property/IsAvailable/{model.Id}", apiToken, parameters);
            return result.IsAvailable;
        }

        public List<ReservedDate> GetReservedDates(IsPropertyAvailableModel model)
        {
            string apiToken = this.DoLogin();
            return this.ExecuteRestAPI<List<ReservedDate>>(Method.GET, $"api/Property/GetReservedDatesByProperty/{model.Id}", apiToken);
        }

        public List<KeyValueModel> AmenitiesList(ref string apiToken)
        {
            if (string.IsNullOrEmpty(apiToken)) apiToken = this.DoLogin();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("Language", (int)Enums.Language.English);

            return this.ExecuteRestAPI<List<KeyValueModel>>(Method.GET, "api/Amenity/List", apiToken, parameters);
        }

        public List<KeyValueModel> CategoryList(ref string apiToken)
        {
            if (string.IsNullOrEmpty(apiToken)) apiToken = this.DoLogin();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("Language", (int)Enums.Language.English);

            return this.ExecuteRestAPI<List<KeyValueModel>>(Method.GET, "api/Category/List", apiToken, parameters);
        }

        public List<string> ClassList(ref string apiToken)
        {
            if (string.IsNullOrEmpty(apiToken)) apiToken = this.DoLogin();
            return this.ExecuteRestAPI<List<string>>(Method.GET, "api/Class/List", apiToken);
        }

        public List<ListCommunityModel> CommunityList(ref string apiToken)
        {
            if (string.IsNullOrEmpty(apiToken)) apiToken = this.DoLogin();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("Language", (int)Enums.Language.English);

            return this.ExecuteRestAPI<List<ListCommunityModel>>(Method.GET, "api/Community/List", apiToken, parameters);
        }

        public List<string> TypeList(ref string apiToken)
        {
            if (string.IsNullOrEmpty(apiToken)) apiToken = this.DoLogin();
            return this.ExecuteRestAPI<List<string>>(Method.GET, "api/Type/List", apiToken);
        }

        public List<KeyValueModel> ZoneList(ref string apiToken)
        {
            if (string.IsNullOrEmpty(apiToken)) apiToken = this.DoLogin();

            Dictionary<string, object> parameters = new Dictionary<string, object>();
            parameters.Add("Language", (int)Enums.Language.English);

            return this.ExecuteRestAPI<List<KeyValueModel>>(Method.GET, "api/Zone/List", apiToken, parameters);
        }
    }
}
