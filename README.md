# VHC API - PROJECT EXAMPLES

## dotnetcore folder (ASP.NET Core Project)
This is a ASP.NET Core MVC basic application. You just need to open `VHC.Web.Example.sln` solution and build/run the project.
Please remember to edit the *dotnetcore/VHC.Web.Example/appsettings.json* file with the right API URL and your API Key/API Secret.

There is a docker-compose pre-configured for the application. Just use `docker-compose up -d` inside *dotnetcore* folder.

## node folder (Node.JS Project)
The Node.JS project is only plain JavaScript files as a console application. You only need to enter in *node* folder and run the following command to get all the required components:
``
npm install
``

The project files are inside the *node/js* folder. Just use `node propertyList.js` _(for example)_ to run the application.
Please remember to edit the *node/js/config.js* file with the right API URL and your API Key/API Secret.

## php folder (PHP 7.4 Project)
The PHP project requires that you have *composer* installed. Open the *php/src* folder and run the following command to install all the required components:
``
composer install
``

Please remember to edit the *php/src/includes/config.php* file with the right API URL and your API Key/API Secret.

If you want, you can run the project via docker-compose too. Just run `docker-compose up -d` inside *php/docker* folder.